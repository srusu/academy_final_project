# Stage 1 - build the app
FROM node:16.10.0-alpine as build-app
WORKDIR /usr/src/react-academy-app
COPY package.json package-lock.json ./
RUN npm install
COPY . ./
RUN npm run build

# Stage 2 - create production image
FROM nginx:1.21.3-alpine
COPY --from=build-app /usr/src/react-academy-app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
